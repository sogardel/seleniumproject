package selenium;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AssertionExample {
	
	@Test(priority =0)
	
	public void softAssertExample() {
		SoftAssert softAssert =  new SoftAssert();
		int num = 2;
		
		System.out.println("Soft Assert incepe aici!");
		
		softAssert.assertTrue(num == 4);
		
		System.out.println("Soft Assert se termina aici!");
		
		softAssert.assertAll();
	}
	
	@Test(priority =1)
	
	public void hardAssertExample() {
		int num = 2;
		System.out.println("Hard Assert incepe aici!");
		assertTrue(num == 4);
		System.out.println("Hard Assert se termina aici!");

	}
	
	

}
