package selenium.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import selenium.utils.BaseTest;

public class ListExample extends BaseTest{
	
	
	
	@Test
	public void test() throws InterruptedException {
		
		driver.get("https://keybooks.ro/team/margaret-robins/");
		Thread.sleep(5000);
		
		List<WebElement> skills = driver.findElements(By.xpath("//div[@class='sc_skills_total']"));
		
		System.out.println("Size :" + skills.size());
		
		System.out.println("Skill 1 :" + skills.get(0).getText());
		System.out.println("Skill 2 :" + skills.get(1).getText());
		System.out.println("Skill 3 :" + skills.get(2).getText());

		
		
		
	}

	
	
	
	
}
