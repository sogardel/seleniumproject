package selenium.tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import selenium.pages.NavigationMenuPage;
import selenium.pages.ShopPage;
import selenium.utils.BaseTest;

public class DropdownTest extends BaseTest{
	
	
	@Test
	public void dropdownExample() throws InterruptedException {
		NavigationMenuPage navPage = new NavigationMenuPage(driver);
		navPage.navigateTo(navPage.shopLink);
		//
		ShopPage shopPage =  new ShopPage(driver);
		shopPage.filterByValue("date");
		Thread.sleep(5000);
		assertEquals(shopPage.getCurrentFilterValue(), "Sort by latest");
		shopPage.filterByVisibleText("Sort by price: high to low");
		assertEquals(shopPage.getCurrentFilterValue(), "Sort by price: high to nimic");
		shopPage.filterByIndex(2);
		assertEquals(shopPage.getCurrentFilterValue(), "Sort by average rating");

	}
	

}
