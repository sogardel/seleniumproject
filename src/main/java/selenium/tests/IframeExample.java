package selenium.tests;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import selenium.pages.ContactPage;
import selenium.pages.NavigationMenuPage;
import selenium.utils.BaseTest;

public class IframeExample extends BaseTest {

	
	@Test
	public void iframeTest() throws InterruptedException {
		NavigationMenuPage menu =  new NavigationMenuPage(driver);
		ContactPage contactPage =  new ContactPage(driver);
		menu.navigateTo(menu.contactLink);
		contactPage.zoomMap(contactPage.zoomInButton);
		Thread.sleep(5000);	
		System.out.println(driver.getWindowHandles());
		contactPage.redirectMap();
		Thread.sleep(5000);	
		List<String> browserTabs = new ArrayList<>(driver.getWindowHandles());
		System.out.println(browserTabs);
		driver.switchTo().window(browserTabs.get(1));
		driver.close();
		Thread.sleep(8000);	

		
		
	}
	
	
}
