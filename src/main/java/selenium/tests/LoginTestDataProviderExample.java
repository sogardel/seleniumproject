package selenium.tests;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import selenium.pages.LoginPage;
import selenium.pages.NavigationMenuPage;
import selenium.utils.BaseTest;
import selenium.utils.Log;
import selenium.utils.TestNgListener;

@Listeners(TestNgListener.class)
public class LoginTestDataProviderExample extends BaseTest{
	
	@DataProvider(name = "loginTestData")
	public Object[][] loginTestData() {
		
		Object[][] data = new Object[4][2];
		
		data[0][0] = "TestUser";
		data[0][1] = "12345@67890";

		data[1][0] = "test.test";
		data[1][1] = "test.test@123";
		
		data[2][0] = "test.test";
		data[2][1] = "test.test@123";
		
		data[3][0] = "test.test";
		data[3][1] = "test.test@12";
		
		return data;
	}
	
	
	
	@Test(dataProvider = "loginTestData")
	public void loginTest(String username, String password) {
		
		NavigationMenuPage menu =  new NavigationMenuPage(driver);
		menu.navigateTo(menu.loginLink);
		Log.info("navigated to login");
		LoginPage loginPage = new LoginPage(driver);
		loginPage.loginInApp(username, password);
		Log.info("try to login with username " + username + " and password " + password);
		assertTrue(driver.findElement(loginPage.logoutButton).isDisplayed());
		Log.info("verify logout button is present");
		driver.findElement(loginPage.logoutButton).click();
		Log.info("click on logout button");

	}
	
	

}
