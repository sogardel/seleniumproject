package selenium.tests;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import selenium.pages.NavigationMenuPage;
import selenium.pages.ShopPage;
import selenium.utils.BaseTest;

public class ActionClassExample extends BaseTest{
	
	//@Test
	public void hoverExample() throws InterruptedException {
		NavigationMenuPage navPage =  new NavigationMenuPage(driver);
				
		navPage.hoverMenu(navPage.aboutLink);
		navPage.hoverMenu(navPage.blogLink);
		navPage.navigateTo(navPage.postFormatsLink);
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/category/post-formats/");
	}
	
	//@Test
	public void dragAndDropExample() throws InterruptedException {
		NavigationMenuPage navPage =  new NavigationMenuPage(driver);
		navPage.navigateTo(navPage.shopLink);
		ShopPage shopPage = new ShopPage(driver);
		
		shopPage.dragAndDropSlider(shopPage.firstSlider, 100, 0);
		shopPage.dragAndDropSlider(shopPage.lastSlider, -100, 0);
		shopPage.clickOnButton(shopPage.filterButton);
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/?min_price=13&max_price=15");
	}
	@Test
	public void doubleClickExample() {
		NavigationMenuPage navPage =  new NavigationMenuPage(driver);
		navPage.navigateTo(navPage.shopLink);
		ShopPage shopPage = new ShopPage(driver);
		
		shopPage.doubleClick(shopPage.addToCartButton);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(shopPage.cartItemIcon), "2"));
		assertEquals(shopPage.getElementText(shopPage.cartItemIcon), "2");
	}
	

}
