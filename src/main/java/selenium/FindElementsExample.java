package selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class FindElementsExample extends BaseTest{
	
	
	@Test
	public void findElementsExample() {
		
		List<WebElement> books = driver.findElements(By.cssSelector("h3[class*='sc_title']"));
		//books.get(2).click();
		for(WebElement element : books) {
			
			System.out.println(element.getText());
		}
		
	}

}
