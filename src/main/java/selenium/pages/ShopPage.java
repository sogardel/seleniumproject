package selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShopPage {
	
	public WebDriver driver;
	public WebElement dropdown;//este doar declarat- deci nu are valoare
	public Select selectDropdown;
	
	
	public ShopPage(WebDriver driver) {
		this.driver =  driver;
	}
	//locators
	public By dropdownLocator = By.name("orderby");
	public By firstSlider = By.cssSelector("span[style='left: 0%;']");
	public By lastSlider = By.cssSelector("span[style='left: 100%;']");
	public By filterButton = By.cssSelector("button[class=button]");
	public By addToCartButton = By.cssSelector("a[class*=add_to_cart_button]");
	public By cartItemIcon = By.className("cart_items");
	
	
	public String getElementText(By locator) {
		
		return driver.findElement(locator).getText();
	}
	
	//methods
	public void filterByValue(String value) {
		dropdown =  driver.findElement(dropdownLocator);// primeste valoare. dropdown se identifica cu driver.findElement(dropdownLocator)
		Select selectDropdown =  new Select(dropdown);
		selectDropdown.selectByValue(value);
	}
	
	public void filterByIndex(int index) {		
		dropdown =  driver.findElement(dropdownLocator);
		selectDropdown =  new Select(dropdown);
		selectDropdown.selectByIndex(index);
	}
	
	public void filterByVisibleText(String text) {
		dropdown =  driver.findElement(dropdownLocator);
		selectDropdown =  new Select(dropdown);
		selectDropdown.selectByVisibleText(text);
	}
	
	public String getCurrentFilterValue() {
		dropdown =  driver.findElement(dropdownLocator);
		selectDropdown =  new Select(dropdown);
		return selectDropdown.getFirstSelectedOption().getText();
		
	}
	
	public void dragAndDropSlider(By locator, int x, int y) {
		WebElement element = driver.findElement(locator);
		Actions actions =  new Actions(driver);
		actions.dragAndDropBy(element, x, y).perform();
		actions.clickAndHold(element).moveToElement(element).release().build().perform();
		
	}
	
	public void clickOnButton(By locator) {
		WebElement element = driver.findElement(locator);
		WebDriverWait wait =  new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	public void doubleClick(By locator) {
		WebElement element = driver.findElement(locator);
		Actions actions = new Actions(driver);
		actions.doubleClick(element).perform();
		
	}
	

}