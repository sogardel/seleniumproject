package selenium.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.google.common.io.Files;

public class TestNgListener implements ITestListener {

	@Override
	public void onTestStart(ITestResult result) {
		Log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.info("+++++++++++++++++++ Start method : "+ result.getName()+ "++++++++++++++++++++++++");
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.info("+++++++++++++++++++Start time :"+ timeStamp + "++++++++++++++++++++++++");
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		Log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.info("+++++++++++++++++++ End method : "+ result.getName()+ "++++++++++++++++++++++++");
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.info("+++++++++++++++++++End time :"+ timeStamp + "++++++++++++++++++++++++");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		Log.error("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Log.error("+++++++++++++++++++ Failure: "+ result.getName()+ "++++++++++++++++++++++++");
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		Log.error("+++++++++++++++++++Fail time :"+ timeStamp + "++++++++++++++++++++++++");
		TakesScreenshot poza = (TakesScreenshot)BaseTest.driver;
		File screenshot = poza.getScreenshotAs(OutputType.FILE);
		try {
			Files.copy(screenshot, new File("screenshots/"+ result.getName()+ timeStamp +".png"));
			Log.info("Saved screenshot : " + "screenshots/"+ result.getName()+ timeStamp +".png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.error(result.getThrowable());
	}

	
	
}
